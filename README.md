#BF Javascript Agegate Wordpress Plugin
##Build Process
You can use the CSS that is built in the public directory, or you can change the styles by editing ```src/scss/agegate.scss```.  It is using Bourbon and Neat for the base styles.

If you change the HTML structure of the agegate template you will need to update the JS and the styles.

NPM: 6.14.15
Node: 14

```
git clone <repo> working-dir
cd working-dir
npm install
npm run [dev|watch|prod]
```

##Basic Install Process
1. Clone repo into plugins directory
2. Update ```agegate-template.php``` with the language and links appropriate for your brand

##Update Process (for plugin maintainer) 10-10
1. Clone b-f-javascript-agegate repo into its own directory
2. Copy b-f-javascript-agegate/src into bf-wp-js-agegate/src (overwrite files)
3. Copy & Paste contents from b-f-javascript-agegate/src/js/agegate.js into designated area of src_ag/js/agegate.js
	-this means below line 51 and above line 53 as of 10/10/17
4. Remove hideAgegate and showAgegate functions from the pasted JS code (we want to override with versions from wp_js_functions.js)
5. Add the jQuery('link[id=agegate-styles-css]')[0].disabled=true; line after "use strict" in document ready
6. Copy src_ag/js/agegate.js over src/js/agegate.js.
7. Run npm install
8. Run npm run prod
9. Copy agegate-outer div from example.php into agegate_template.php
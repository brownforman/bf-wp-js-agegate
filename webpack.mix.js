let mix = require('laravel-mix');
// let dirstuff = __dirname+'/node_modules/normalize-libsass';
// console.log(dirstuff);
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('src/js/agegate.js', 'dist/js')
  .sass('src/scss/app.scss', 'dist/css');

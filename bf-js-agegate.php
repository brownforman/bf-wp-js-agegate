<?php
/**
 * Plugin Name: B-F Javascript Agegate
 * Description: Agegate that pops over the content using a JS lightbox
 * Author:      BF
 * Author URI:
 * Version:     1.0
 *
 * @package   B-F Javascript Agegate
 * @version   1.0
 * @author    BF
*/

// Prevent direct access
if ( ! defined( 'ABSPATH' ) ) exit;

// Global object
$bf_agegate = new BF_JS_Agegate();

class BF_JS_Agegate {

	/**
	 ** Sets up everything.
	 **/
	public function __construct() {
		$this->setup_globals();

		//Disable this conditional check if the site will be on Cloudflare
		//if(@$_COOKIE['lda'] != "1"){
			$this->setup_actions();
		//}
	}

	private function setup_globals() {
		// Directories and URLs
		$this->file       = __FILE__;
		$this->basename   = plugin_basename( $this->file );
		$this->plugin_dir = plugin_dir_path( $this->file );
		$this->plugin_url = plugin_dir_url( $this->file );
	}

	private function setup_actions() {
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'wp_footer', array( $this, 'render_html') );
	}

	public function render_html(){
		//PHP variables are available to the template
		ob_start();
		include $this->plugin_dir .'agegate_template.php';
		$file = ob_get_contents();
		ob_end_clean();
		echo $file;
	}

	public function enqueue_styles() {
		wp_enqueue_style( 'agegate-styles', $this->plugin_url . 'dist/css/app.css' );
	}

	public function enqueue_scripts() {
		wp_enqueue_script( 'jquery' );
		wp_register_script('agegate-script', $this->plugin_url . 'dist/js/agegate.js', array('jquery'), NULL, true);
		wp_localize_script('agegate-script', 'bfa',
			array(
				'altMethod'=>false,
				'excludeJSON'=>plugin_dir_url(dirname(__FILE__)).'bf-wp-js-agegate/dist/js/exclude.json'
			)
		);
		wp_enqueue_script( 'agegate-script');
	}
}

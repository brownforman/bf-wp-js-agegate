(function($){

function showAgegate(){
	setAutoTabbing();
	setCountryBehaviors();
	setMaxYear();
	$('link[id=agegate-styles-css]')[0].disabled=false;
	if ($('div#body-outer').length > 0) {
		$('div#agegate-outer').css('display', 'block');
		$('div#body-outer').css('display', 'none');
	} else {
		//JBG
		bfa.altMethod = true;
		var ag = $('div#agegate-outer').detach();
		$('body').wrapInner('<div id="body-outer"></div>');
		ag.appendTo('body');
		$('div#agegate-outer').css('display', 'block');

		$('div#body-outer').css('visibility', 'hidden').addClass('stop-scrolling');
	}
	$('html').addClass('agegate');
}

function hideAgegate(){
	$('form[name=agegate]').attr('disabled', 'disabled');
	$('div#agegate-outer').css('display', 'none');
	$('div#body-outer').removeClass('stop-scrolling').css('visibility', 'visible')
	if (bfa.altMethod) {
		$('div#body-outer').children().first().unwrap();
	}

	$('link[id=agegate-styles-css]')[0].disabled=true;

	if (!window.location.hash) {
        window.scrollTo(0, 0);
        document.body.scrollTop = 0;
	} else {
		var h = window.location.hash.split('#')[1];
	    var top = $('[name='+h+']').offset().top;
	    window.scrollTo(0, top);                        //Go there directly or some transition
	}
}

function getExcludedJson(){
	//Use an external JSON file
	return $.getJSON(bfa.excludeJSON);

	//OR
	//Set JSON in Code
	/*
	return {
		"paths": [
			"privacy",
			"privacy-policy"
		]
	}
	*/
}

/* b-f-javascript-agegate code paste here, without showAgegate, hideAgegate, getExcludedJson functions */

/* end b-f-javascript-agegate code */
/* add this line after "use strict" in document ready */
/* jQuery('link[id=agegate-styles-css]')[0].disabled=true; */
}(jQuery));
